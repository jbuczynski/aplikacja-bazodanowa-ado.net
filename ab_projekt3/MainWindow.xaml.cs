﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;



namespace ab_projekt3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();



            //GridData to wskaznik na kontrolke xamla
            ObslugaPolaczenia = new ObslugaConn(str);
            ObslugaPolaczenia.Wykonaj(start, "", ListaTabel, "read");
           // ObslugaPolaczenia.Start(start,"",ListaTabel,"read");
        

          
          //DataGrid GridData1 = GridData;
        }
        
        string str = @"Data Source=laptop\sqlexpress; Initial Catalog=Dziennik;Integrated Security=True";
        string start = "SELECT TABLE_NAME FROM information_schema.tables;";

        ObslugaConn ObslugaPolaczenia;
       
             
       


        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Polaczenie_Click(object sender, RoutedEventArgs e)
        {
           

            MessageBox.Show("dzila funkcja iobslugujaca klikniecie");

        }

        private void Zmien_Polaczenie_Domyslne_Click(object sender, RoutedEventArgs e)
        {
            

           
        }
            

        private void ListaTabel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
                                
            
            ListBox lb = sender as ListBox;
            DataRowView view = lb.SelectedItem as DataRowView;

            string selected = (string)view["TABLE_NAME"];

            string str1 = "select * from " + selected + ";";
            string str2 =  selected ;
            GridData.CurrentTableName = selected;
            MessageBox.Show(selected);


            try
            {
                if (ObslugaPolaczenia.sprawdz_tryb() == "bezpolaczeniowy" && ObslugaPolaczenia.set.Tables.Count == 0)
                {
                    BezpolaczenioweConn BPC = new BezpolaczenioweConn(str);
                    BPC.FillDataSet_by_ListBox(ObslugaPolaczenia.set, this.ListaTabel);
                }

                if (ObslugaPolaczenia.sprawdz_tryb() == "bezpolaczeniowy" && ObslugaPolaczenia.set.Tables.Count != 0) 
                {
                    SqlConnection con = new SqlConnection(str);
                    con.Open();

                    string sql = "SELECT* From mar_miasto";
                            SqlDataAdapter adapter = new SqlDataAdapter(sql, con );
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
                     
                           adapter.Update(ObslugaPolaczenia.set.Tables[0]);
                            con.Close();
                }


                ObslugaPolaczenia.Wykonaj(str1, str2, GridData, "read");
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Opercaj nie powiodla sie " + ex.Message);
                return;
            }
           

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

        }
        private void ChangeMode_Click(object sender, RoutedEventArgs e)
        {
         
            Window1 InputBox = new Window1(ObslugaPolaczenia, SynchButton);
            InputBox.Show();
         
        }

        private void GridData_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {

          //  DataGridViewCellCollection komorki = dataGridView1.Rows[e.RowIndex].Cells;


             MyDataGrid dg = sender as MyDataGrid;
             string element = ((TextBox)e.EditingElement).Text;

             //creating sql query 
           
              //   DataGridHelper.

            MessageBox.Show("Biezacy Index: " + (dg.Items.IndexOf(dg.CurrentItem)+1) + "dg.Items.Count - 1: " + (dg.Items.Count - 1));
            try{
            if (dg.Items.IndexOf(dg.CurrentItem)+1 != dg.Items.Count - 1)
           {
             String tekst;
             string ID = ((TextBlock)(dg.Columns.First().GetCellContent((DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex))))).Text;
             if(element.All(char.IsDigit))
                 tekst = "UPDATE " + dg.CurrentTableName + " SET " + ((DataGridColumn)e.Column).Header + "=" + element + " WHERE " + dg.Columns.First().Header + "=" + ID;
                else
                 tekst = "UPDATE " + dg.CurrentTableName + " SET " + ((DataGridColumn)e.Column).Header + "=" + "'" + element + "'" + " WHERE " + dg.Columns.First().Header + "=" + ID;
                MessageBox.Show(tekst);

                //creating linq_query 
                string str2 = (dg.CurrentTableName + " " + ((DataGridColumn)e.Column).Header + " " +e.Row.GetIndex() + " " + element);


                


                ObslugaPolaczenia.Wykonaj(tekst,str2, GridData, "update");


            }
        }
             catch(SqlException ex)
               {
                   MessageBox.Show("Opercaj nie powiodla sie" + ex.Message);
                   return;
                }

        
                  

        }

    
        private void GridData_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            MyDataGrid dg = sender as MyDataGrid;
            try
            {

                if (dg != null)
                {
                    DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex));
                    if (e.Key == Key.Delete && !dgr.IsEditing)
                    {
                        // User is attempting to delete the row
                        var result = MessageBox.Show(
                       "Napewno usunac zaznaczony wiersz.\n\nNAPEWNO?!!?",
                       "USUN!",
                       MessageBoxButton.YesNo,
                       MessageBoxImage.Question,
                       MessageBoxResult.No);
                        //Jesli nie to nie 
                        if (result == MessageBoxResult.No)
                        {
                            e.Handled = (result == MessageBoxResult.No);
                            return;
                        }



                        string ID = ((TextBlock)(dg.Columns.First().GetCellContent((DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex))))).Text;

                        String tekst = "DELETE FROM " + dg.CurrentTableName + " WHERE " + dg.Columns.First().Header.ToString() + "=" + ID;

                        MessageBox.Show(tekst);



                        string str2 = (dg.CurrentTableName + " " + dg.SelectedIndex);



                        ObslugaPolaczenia.Wykonaj(tekst, str2, GridData, "delete");

                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Opercaj nie powiodla sie" + ex.Message);
                return;
            }
        }

        private void GridData_RowEditEnding_1(object sender, DataGridRowEditEndingEventArgs e)
        {



           MyDataGrid dg = sender as MyDataGrid;
           int selected = dg.SelectedIndex;
           DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(selected)); //Jawale co to wgl bylo, kosmos




            //omijanie ograniczeń transakcji, zdjecie metody obslugujacej zdarzenie, potwierdzenie obecnej transakcji(dzieki zdjeciu metody nie ywwola sie lancuh),
             //odswiezenie itemow po updacie dodanie metody spowrotem, dalsze dzialania
            
         
          
               (sender as DataGrid).RowEditEnding -= GridData_RowEditEnding_1;
               (sender as DataGrid).CommitEdit();
               (sender as DataGrid).Items.Refresh();
               (sender as DataGrid).RowEditEnding += GridData_RowEditEnding_1;

             
               //string element = ((TextBox)e.EditingElement).Text; 

               try
               {
                   if (e.EditAction == DataGridEditAction.Commit)
                   {
                       if (dg.Items.IndexOf(dg.CurrentItem) + 1 == dg.Items.Count - 1)
                       {


                           //  dg.Dispatcher.BeginInvoke(new Action(() => dg.Items.Refresh()), System.Windows.Threading.DispatcherPriority.ApplicationIdle);
                           DataGridColumn c = dg.Columns[0];

                           foreach (var column in dg.Columns)
                           {
                               if (string.IsNullOrEmpty(((TextBlock)(column.GetCellContent(dgr))).Text))
                               {
                                   MessageBox.Show("zostało puste pole, uzupełnij je");
                                   (sender as DataGrid).CancelEdit();
                                   return;
                               }
                           }

                           string values = "";
                           string columns_names = "";

                           int licznik = 0;
                           foreach (var column in dg.Columns)
                           {
                               if (licznik != 0)
                               {
                                   string element = ((TextBlock)(column.GetCellContent(dgr))).Text;

                                   if (element.All(char.IsDigit))
                                   {
                                       values += ((TextBlock)(column.GetCellContent(dgr))).Text + ", ";
                                   }
                                   else
                                       values += "'" + ((TextBlock)(column.GetCellContent(dgr))).Text + "'" + ", ";
                               }
                               licznik++;
                           }

                           licznik = 0;
                           foreach (var column in dg.Columns)
                           {
                               if (licznik != 0)
                               {
                                   columns_names += column.Header + ", ";
                               }
                               licznik++;
                           }

                           values = values.Substring(0, (values.Length - 2));

                           columns_names = columns_names.Substring(0, (columns_names.Length - 2));

                           String tekst = "INSERT INTO " + dg.CurrentTableName + "(" + columns_names + ") VALUES(" + values + ")";

                           MessageBox.Show(tekst);
                           string str2 = dg.CurrentTableName + " " + (dg.Items.IndexOf(dg.CurrentItem)) + " " + columns_names + " " + values;




                           ObslugaPolaczenia.Wykonaj(tekst, str2, GridData, "insert");
                       }

                   }
               }

           
           catch(SqlException ex)
               {
                   MessageBox.Show("Opercaj nie powiodla sie" + ex.Message);
                   return;
                }

         }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
    
        }

        private void SynchButton_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection con = new SqlConnection(str);
            con.Open();

            
           

            foreach(DataTable table in ObslugaPolaczenia.set.Tables)
            {
                try
                {

                    string sql = "SELECT* From " + table.TableName;
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);

                    adapter.Update(table);
                   
                }

                catch(SqlException ex)
                {
                    MessageBox.Show("Blad sql" + ex.Message);
                }
          
            }
            MessageBox.Show("Synchronizacja zakonczona");
            con.Close();
          
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SqlConnection con = new SqlConnection(str);
            con.Open();




            foreach (DataTable table in ObslugaPolaczenia.set.Tables)
            {

             try
                {

                    string sql = "SELECT* From " + table.TableName;
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);

                    adapter.Update(table);
                
                }
                catch(SqlException ex)
                {
                    MessageBox.Show("Blad sql" + ex.Message);
                }
          
               
            }
            MessageBox.Show("przed zamknieciem zsynchronizowano z baza danych");
            con.Close();
        }



      

    }


}
