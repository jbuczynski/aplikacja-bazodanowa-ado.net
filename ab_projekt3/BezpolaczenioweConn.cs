﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ab_projekt3
{
    public class BezpolaczenioweConn
    {

        public BezpolaczenioweConn(string constr, string sqlquery, DataSet set)
        {

            con = new SqlConnection(constr);
            // adapter = new SqlDataAdapter(sqlquery, con);
            query = sqlquery;
            dataset = set;
        }

        public BezpolaczenioweConn(string constr)
        {

            con = new SqlConnection(constr);

        }
        private string query;
        SqlConnection con;
        SqlDataAdapter adapter;
        DataTable dt = new DataTable();
        DataSet dataset;
        // void synchronizuj( Selector kontrolka, SqlConnection conn)
        // {


        // }

        private childItem FindVisualChild<childItem>(DependencyObject obj)
        where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }


        public void FillDataSet_by_ListBox(DataSet set, ListBox zrodlo)
        {


            try
            {

                for (int i = 0; i < zrodlo.Items.Count; i++)
                {
                    // 1. declare command object with parameter



                    ListBoxItem myListBoxItem =
                        (ListBoxItem)(zrodlo.ItemContainerGenerator.ContainerFromItem(zrodlo.Items[i]));

                    // Getting the ContentPresenter of myListBoxItem
                    ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(myListBoxItem);

                    // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                    DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                    TextBlock myTextBlock = (TextBlock)myDataTemplate.FindName("List_Text_box", myContentPresenter);

                    // Do something to the DataTemplate-generated TextBlock
                    //   MessageBox.Show("Iteruje po textboxach listboxie!!!"
                    //     + myTextBlock.Text);



                    string cmd = " SELECT * FROM ";
                    string table_name = myTextBlock.Text;

                    cmd += table_name;

                  //  MessageBox.Show(cmd);

                    adapter = new SqlDataAdapter(cmd, con);
                    DataTable dt = new DataTable();
                    dt.TableName =  table_name  ;
                    adapter.Fill(dt);
                    set.Tables.Add(dt);



                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("FILLL!"+ex.Message);
            }


        }


        public void ReadData(string query, Selector ekran)
        {

            //adapter.Fill(dt);
            //  FillDataSet_by_DataTable(dataset, dt);



            try
            {
              //  MessageBox.Show(dataset.Tables.Count.ToString());
              //  MessageBox.Show(dataset.Tables[0].TableName.ToString());

                DataView dv = dataset.Tables[query].DefaultView;
                ekran.ItemsSource = dv;
                ekran.Items.Refresh();
            }

            catch (Exception ex)
            {
                MessageBox.Show("!!!!!!!!!!!!!"+ex.Message);
            }

        }

        //update data Set
        public void Update(string query, Selector ekran)
        {

            string[] words = query.Split(' ');

            string NazwaTabeli, NazwaKolumny, IdWiersza, Wartosc;
            try
            {

                NazwaTabeli = words[0];
                NazwaKolumny = words[1];
                IdWiersza = words[2];
                Wartosc = words[3];

                dataset.Tables[NazwaTabeli].Rows[int.Parse(IdWiersza)][NazwaKolumny] = Wartosc;

            }

            catch (Exception ex)
            {
                MessageBox.Show("bład w przypisaniu wartosci lub inny, podczas uaktualniania dataset" + ex.Message);
            }

            //insert
            //    table.Rows.Add("cat", DateTime.Now);
            // Rows[IdWiersza][NazwaKolumny] = Wartosc;




        }

        public void Delete(string query, Selector ekran)
        {

            string[] words = query.Split(' ');

            string NazwaTabeli,  IdWiersza;
            try
            {

                NazwaTabeli = words[0];
                IdWiersza = words[1];
             

                dataset.Tables[NazwaTabeli].Rows[int.Parse(IdWiersza)].Delete();

            }

            catch (Exception ex)
            {
                MessageBox.Show("bład w przypisaniu wartosci lub inny, podczas uaktualniania dataset" + ex.Message);
            }

            //insert
            //    table.Rows.Add("cat", DateTime.Now);
            // Rows[IdWiersza][NazwaKolumny] = Wartosc;




        }

        public void Insert(string query, Selector ekran)
        {

            //string[] words = query.Split(' ');

            //string NazwaTabeli, NazwaKolumny, IdWiersza, Wartosc;
            //try
            //{

            //    NazwaTabeli = words[0];           
            //    IdWiersza = words[1];


            //    int len = words.Length;
            //   // string[] kolumny = new string[(len-2)/2];
            //    string[] wartosci = new string[(len - 2) / 2];

            //    for (int i = 2; i < len/2 + 1 ; i++)
            //    {
            //        //kolumny[i - 2] = words[i];
            //        wartosci[i - 2] = words[i + (len - 2)/2];
            //    }
               

            //    dataset.Tables[NazwaTabeli].Rows[int.Parse(IdWiersza)].ItemArray = wartosci;

            //   // dataset.Tables[NazwaTabeli].Rows[int.Parse(IdWiersza)] = Wartosc;
            //}

            //catch (Exception ex)
            //{
            //    MessageBox.Show("bład w przypisaniu wartosci lub inny, podczas uaktualniania dataset" + ex.Message);
            //}

            ////insert
            ////    table.Rows.Add("cat", DateTime.Now);
            //// Rows[IdWiersza][NazwaKolumny] = Wartosc;




        }

        //synchronizacja data setu z bazą
    }
}