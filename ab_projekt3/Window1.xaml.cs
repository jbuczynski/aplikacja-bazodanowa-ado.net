﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ab_projekt3
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1(ObslugaConn obsl,Button btn)
        {
          
            InitializeComponent( );
          
            ObslugaPolaczenia = obsl;
             btnn = btn;
            
        }
        Button btnn;
       public String text;
       ObslugaConn ObslugaPolaczenia;
        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            // YesButton Clicked! Let's hide our InputBox and handle the input text.
          //  InputBox.Visibility = System.Windows.Visibility.Collapsed;

            // Do something with the Input
            text = InputTextBox.Text;
            ObslugaPolaczenia.zmien_tryb(text,btnn);
            this.Close();
            // Clear InputBox.
          // InputTextBox.Text = String.Empty;
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            // NoButton Clicked! Let's hide our InputBox.
          //  InputBox.Visibility = System.Windows.Visibility.Collapsed;

            // Clear InputBox.
          //  InputTextBox.Text = String.Empty;
            this.Close();
        }

        private void InputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            
        }
    }
    
}
