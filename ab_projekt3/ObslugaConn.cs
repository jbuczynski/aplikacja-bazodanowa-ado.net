﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data;
using System.Windows.Navigation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Drawing;

namespace ab_projekt3
{
   public delegate void TrybChangedEventHandler(object sender, EventArgs e);
   public class tryb
    {
        private string tryb_dzialania{set; get;}
        public event TrybChangedEventHandler Changed;

      public tryb()
        {
            tryb_dzialania = "polaczeniowy";
        }

      // Invoke the Changed event; called whenever tryb is changed
      protected virtual void OnChanged(EventArgs e)
      {
          if (Changed != null)
              Changed(this, e);
      }

       public void ustaw_tryb(string tryb, Button SynchButton)
        {
            MessageBox.Show(tryb);
            if (tryb == "polaczeniowy" || tryb == "bezpolaczeniowy")
            {
                if (tryb == "polaczeniowy")
                    SynchButton.Visibility=  System.Windows.Visibility.Collapsed;
                else
                    SynchButton.Visibility = System.Windows.Visibility.Visible;


                    this.tryb_dzialania = tryb;
             
            }
            else
            {
                throw new System.ArgumentException("zly parametr trybu", "original");
                
            }
        }

       public string sprawdz_tryb()
        {
            return this.tryb_dzialania;
        }

    }

   public class ObslugaConn
    {
       public ObslugaConn(string cos)
       {
           constr = cos;
          
        }
     
      private tryb tryb_dzialania = new tryb();
      private string constr;
     public  DataSet set = new DataSet("offline");
     
      // DataTable dt = new DataTable();
      ////  SqlDataAdapter da = new SqlDataAdapter(cmd);
      ////  da.Fill(dt);
       
      //  wiazanie.ItemsSource = dt.DefaultView; 

     public string sprawdz_tryb()
     {
         return this.tryb_dzialania.sprawdz_tryb();
     }

      public void Wykonaj(string sqlquery,string linq_query, Selector ekran,string rodzaj_operacji )
      {
        
         
          
           string tryb_dzialania1 = tryb_dzialania.sprawdz_tryb();
           if(tryb_dzialania1 == "polaczeniowy")
           {
               
               SqlConnection con = new SqlConnection(constr); 
               PolaczenioweConn polaczenie_polaczeniowe = new PolaczenioweConn();
               if (rodzaj_operacji == "delete" || rodzaj_operacji == "update" || rodzaj_operacji == "insert")
               {
                   rodzaj_operacji = "execute";
               }

               switch (rodzaj_operacji)
               {
                   case "read":
                       polaczenie_polaczeniowe.ReadData(sqlquery, ekran, con);
                       break;
                   case "execute":
                       polaczenie_polaczeniowe.Execute(sqlquery, ekran, con);
                       MessageBox.Show("Wykonano zapytanie");
                       break;
                   default:
                       MessageBox.Show("Nieznany rodzaj operacji");
                       break;


               }
            
           }
           else 
           {
               BezpolaczenioweConn BPC = new BezpolaczenioweConn(constr, sqlquery,set);

               
                
               switch (rodzaj_operacji)
               {


                   case "read":

                       BPC.ReadData(linq_query,ekran);
                     
                       break;
                   case "update":

                       BPC.Update(linq_query,ekran);
                       break;
                   case "insert":

                       BPC.Insert(linq_query, ekran);
                       break;
                   case "delete":

                       BPC.Delete(linq_query, ekran);
                       break;
                   default:
                       MessageBox.Show("Nieznany rodzaj operacji");
                       break;


               }
                       MessageBox.Show("Bezpolaczeniowy");
                       
              
               }
             
              

           }

       public void start()
      {





      }

       private byte[] ImageToByteArray(System.Windows.Controls.Image img)
       {
           ImageConverter converter = new ImageConverter();
           return (byte[])converter.ConvertTo(img, typeof(byte[]));
       }

       private System.Windows.Controls.Image ByteArrayToImage(byte[] byteArray)
       {
           ImageConverter converter = new ImageConverter();
           return (System.Windows.Controls.Image)converter.ConvertFrom(byteArray);
       }
 

    

       public void zmien_tryb(string tryb,Button btn)
       {
           tryb_dzialania.ustaw_tryb(tryb,btn);
       }




    }
}
