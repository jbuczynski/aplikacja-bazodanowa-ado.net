﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace ab_projekt3
{
   public class PolaczenioweConn
    {

       public void ReadData(string sqlquery, Selector kontrolka, SqlConnection conn)
     {
         IDataReader rdr = null;
 
         try
         {
             // Open the connection
             conn.Open();
 
             // 1. Instantiate a new command with a query and connection
             SqlCommand cmd = new SqlCommand(sqlquery, conn);

             try
             {
                 // 2. Call Execute reader to get query results
                 rdr = cmd.ExecuteReader();
                 DataTable dt = new DataTable();
                
                 dt.Load(rdr);
                 // print the CategoryName of each record
               #region komentarze
                 //  Binding binding = new Binding() { Mode = BindingMode.OneWay, Source = dt, Path = new PropertyPath(".") };
               //binding.TargetNullValue = "NULL";
               //binding.Converter=
               //  BindingOperations.SetBinding(kontrolka, DataGrid.ItemsSourceProperty, binding );

               // kontrolka.DataContext = dt;
                 //  kontrolka.SetBinding(dt,);
                 #endregion
               kontrolka.ItemsSource = dt.DefaultView;
               kontrolka.Items.Refresh();
             }
             catch(SqlException ex)
             {
                 string errorMessages = null;
                 for (int i = 0; i < ex.Errors.Count; i++)
                 {
                     errorMessages = ("Index #" + i + "\n" +
                  "Message: " + ex.Errors[i].Message + "\n" +
                  "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                  "Source: " + ex.Errors[i].Source + "\n" +
                  "Procedure: " + ex.Errors[i].Procedure + "\n");
                 }



                 MessageBox.Show(errorMessages); 
             }
             
         }
         finally
         {
             // close the reader
             if (rdr != null)
             {
                 rdr.Close();
             }
 
             // Close the connection
             if (conn != null)
             {
                 conn.Close();
             }
         } 
     }

          /// <summary>
          /// use ExecuteNonQuery method for Insert
          /// </summary>
          public void Execute(string sqlquery, Selector kontrolka, SqlConnection conn)
          {
             
                  // Open the connection
                  conn.Open();
                 




                  // prepare command string
              
                  // 1. Instantiate a new command with a query and connection
                  SqlCommand cmd = new SqlCommand(sqlquery, conn);

                  // 2. Call ExecuteNonQuery to send command
                  try {
                  cmd.ExecuteNonQuery();
                        }
                  catch (SqlException ex) 
                  {
                      string errorMessages=null;
                      for (int i = 0; i < ex.Errors.Count; i++)
                      {
                                 errorMessages = ("Index #" + i + "\n" +
                              "Message: " + ex.Errors[i].Message + "\n" +
                              "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                              "Source: " + ex.Errors[i].Source + "\n" +
                              "Procedure: " + ex.Errors[i].Procedure + "\n");
                      }



                      MessageBox.Show(errorMessages); 
                  }
            
              finally
              {
                  // Close the connection
                  if (conn != null)
                  {
                      conn.Close();
                  }
              }


          }

   
         
 }


    
}
